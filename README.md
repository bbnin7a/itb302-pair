# Usage 
## Installation ##
```
npm install
```


## Start the programme ###
```
npm start
```

## Automatic Unit Testing ###
```
npm test
```

## Excutable file ##

The programme can be executed without Node.js install.
File location:
```
/root
├── build
│   ├── hktax-calculator-macos
│   ├── hktax-calculator-win.exe
```


Repo:
https://gitlab.com/bbnin7a/itb302-pair

Team members:
Lai Justine (56235545)
Leung Shing Lung (57162398)
Fung King Lok (57132799)

