const inquirer = require('inquirer')
const Tax = require('./lib/Tax')
const ReportBuilder = require('./lib/ReportBuilder')
const { incomeValidator } = require('./lib/helper')
const TaxAdvisor = require('./lib/TaxAdvisor')
const colors = require('colors')

const questions = [
  {
    type: 'list',
    name: 'marital',
    message: 'Please select your marital status?',
    choices: ['Single', 'Marriage'],
    filter(val) {
      return val.toLowerCase()
    },
  },
  {
    type: 'input',
    name: 'income',
    message: "What's your annual income?",
    validate: incomeValidator,
  },
  {
    type: 'input',
    name: 'income1',
    message: "What's the husband annual income?",
    validate: incomeValidator,
  },
  {
    type: 'input',
    name: 'income2',
    message: "What's the wife annual income?",
    validate: incomeValidator,
  },
  {
    type: 'confirm',
    name: 'isRetry',
    message: 'Do you want to try again?',
    default: true,
  },
]

let retry = true

async function main() {
  console.log('Welcome to the Tax Calculator (v1.0)\n'.underline)

  while (retry) {
    let report
    const { marital } = await inquirer.prompt(questions[0])
    // Single
    if (marital === 'single') {
      const { income } = await inquirer.prompt(questions[1])
      const individual = new Tax(income)

      report = new ReportBuilder(individual)
      report.print()
    } else {
      // mariage
      const { income1 } = await inquirer.prompt(questions[2])
      const husband = new Tax(income1)

      const { income2 } = await inquirer.prompt(questions[3])
      const wife = new Tax(income2)

      const joint = new Tax(income1, income2)
      report = new ReportBuilder()
        .add('Husband', husband)
        .add('Wife', wife)
        .add('Joint', joint)
      report.print()
      console.log()
      console.log(colors.bgWhite.black('######## RECOMMENDATION ########\n'))

      const taxAdvisor = new TaxAdvisor(joint.tax, husband.tax + wife.tax)

      console.log(
        `Since Joint is ${taxAdvisor.formatted.joint} and Separate ${taxAdvisor.formatted.separate}`
      )
      if (taxAdvisor.recommend) {
        console.log(
          `If you choose ${
            taxAdvisor.recommend
          }. Tax payable will be ${colors.green(
            'Less ' + taxAdvisor.formatted.moneySave
          )}`
        )
        console.log(
          `We recommend ` +
            colors.yellow(`${taxAdvisor.recommend} tax assessment`) +
            '❗️\n\n'
        )
      } else {
        console.log(
          `There is ${colors.green(
            'no difference'
          )} whether you choose "Joint" or "Separate" tax assessment \n\n`
        )
      }
    }

    const { isRetry } = await inquirer.prompt(questions[4])
    if (!isRetry) {
      retry = false
      console.log('Thanks for using it, much appreciate it\n👋ByeBye')
    }
  }
}

main()
