const { Table } = require('console-table-printer')
const { formatDollar } = require('./helper')
const colors = require('colors')

const columnMap = {
  income: 'Income',
  mpf: 'MPF',
  netTotalIncome: 'Net Total Income',
  netChargeableIncome: 'Net Chargeable Income',
  tax: 'Tax Payable',
}

class ReportBuilder {
  constructor(obj) {
    if (obj) {
      Object.keys(obj).forEach((key) => {
        this[key] = { Individual: obj[key] }
      })
    }
  }
  add(person, obj) {
    Object.keys(obj).forEach((key) => {
      this[key] = { ...this[key], [person]: obj[key] }
    })
    return this
  }

  print() {
    const columns = [{ name: 'data', title: 'Field', alignment: 'left' }]
    Object.keys(this.mpf).forEach((key) => {
      columns.push({ name: key, title: key, alignment: 'right' })
    })
    const p = new Table({
      columns,
    })
    let showStandardRateRemarks = false
    Object.keys(columnMap).forEach((key) => {
      const item = this[key]
      Object.keys(item).forEach((i) => {
        // add `^` symbol in the value of tax
        const isStandardRate = key === 'tax' && this.isStandardRate[i] === true
        if (isStandardRate) {
          showStandardRateRemarks = true
        }
        item[i] = formatDollar(item[i]) + (isStandardRate ? '^' : '')
      })
      if (key === 'tax') {
        p.addRow({ data: columnMap[key], ...item }, { color: 'yellow' })
      } else {
        p.addRow({ data: columnMap[key], ...item })
      }
    })

    console.log(`\n\n${colors.bgWhite.black('######## SUMMARY ########\n')}`)
    p.printTable()

    if (showStandardRateRemarks) {
      console.log(' ^ in standand rate 15% (no allowances) \n')
    }
  }
}

module.exports = ReportBuilder
