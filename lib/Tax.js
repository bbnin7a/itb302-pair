const taxCalculator = require('./TaxCalculator')

class Tax {
  constructor(_income1, _income2 = 0) {
    const income1 = +_income1
    const income2 = +_income2
    const isJoint = income2 !== 0
    this.income = income1 + income2

    if (isJoint) {
      const mpf1 = taxCalculator.calcMPF(income1)
      const mpf2 = taxCalculator.calcMPF(income2)
      this.mpf = mpf1 + mpf2

      const netTotalIncome1 = taxCalculator.calcNetTotalIncome(income1, mpf1)
      const netTotalIncome2 = taxCalculator.calcNetTotalIncome(income2, mpf2)
      this.netTotalIncome = netTotalIncome1 + netTotalIncome2
    } else {
      this.mpf = taxCalculator.calcMPF(this.income)
      this.netTotalIncome = taxCalculator.calcNetTotalIncome(
        this.income,
        this.mpf
      )
    }

    this.netChargeableIncome = taxCalculator.calcNetChargeableIncome(
      this.netTotalIncome,
      isJoint
    )
    // after the allowance
    const sTax = taxCalculator.calcTaxInStandardRate(this.netTotalIncome)
    const pTax = taxCalculator.calcTaxInProgressiveRate(
      this.netChargeableIncome
    )
    this.isStandardRate = sTax < pTax

    this.tax = this.isStandardRate ? sTax : pTax
  }
}

module.exports = Tax
