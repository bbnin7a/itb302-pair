const { formatDollar } = require('./helper')

class TaxAdvisor {
  constructor(jointTax, separateTaxSum) {
    const diff = jointTax - separateTaxSum

    if (diff < 0) {
      this.recommend = 'Joint'
    } else if (diff > 0) {
      this.recommend = 'Separate'
    } else {
      this.recommend = null
    }

    this.moneySave = Math.abs(diff)
    this.formatted = {
      joint: formatDollar(jointTax),
      separate: formatDollar(separateTaxSum),
      moneySave: formatDollar(this.moneySave),
    }
  }
}

module.exports = TaxAdvisor
