const _ = require('lodash')

class TaxCalculator {
  constructor() {
    this.RATE_PROGRESSIVE_BRACKET = [
      {
        value: 50000,
        rate: 0.02,
      },
      {
        value: 50000,
        rate: 0.06,
      },
      {
        value: 50000,
        rate: 0.1,
      },
      {
        value: 50000,
        rate: 0.14,
      },
      {
        value: null,
        rate: 0.17,
      },
    ]
    this.RATE_STANDARD = 0.15
    this.MPF_RATE = 0.05
    this.MPF_CAPPED = 18000
    this.ALLOWANCE_SINGLE = 132000
    this.ALLOWANCE_MARRIAGE = 264000
    this.INCOME_NO_MPF_REQUIRED = 7100 * 12 // not required MPF contributions
  }

  // calculate mpf
  calcMPF(income) {
    if (income <= this.INCOME_NO_MPF_REQUIRED) {
      return 0
    }
    const mpf = +income * this.MPF_RATE
    return mpf <= this.MPF_CAPPED ? mpf : this.MPF_CAPPED
  }

  // calculate net total income
  calcNetTotalIncome(income, mpf) {
    return income - mpf
  }

  // calculate net chargeable income
  calcNetChargeableIncome(income, isMarriage = false) {
    if (isMarriage) {
      const nci = income - this.ALLOWANCE_MARRIAGE
      return nci > 0 ? nci : 0
    }
    const nci = income - this.ALLOWANCE_SINGLE
    return nci > 0 ? nci : 0
  }

  // calculate final tax payable
  calcTaxPayable(income) {
    const sTax = this.calcTaxInStandardRate(income)
    const pTax = this.calcTaxInProgressiveRate(income)
    return pTax < sTax ? pTax : sTax
  }

  calcTaxInStandardRate(income) {
    return _.round(income * this.RATE_STANDARD, 2)
  }

  calcTaxInProgressiveRate(income) {
    let crntIncomeLoop = income
    const taxRates = [...this.RATE_PROGRESSIVE_BRACKET]
    let tax = 0
    for (let i = 0; i < taxRates.length; i++) {
      const taxRate = taxRates[i]
      // loop break with income loop is within taxRate.value
      if (crntIncomeLoop < taxRate.value || taxRate.value === null) {
        tax += crntIncomeLoop * taxRate.rate
        break
      }
      // accumulate tax by its max
      tax += taxRate.value * taxRate.rate
      // prepare for next loop
      crntIncomeLoop -= taxRate.value
    }
    return _.round(tax, 2)
  }
}

const taxCalculator = new TaxCalculator()
Object.freeze(taxCalculator)
module.exports = taxCalculator
