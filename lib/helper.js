const formatDollar = (num) => {
  var p = num.toFixed(2).split('.')
  return (
    '$' +
    p[0]
      .split('')
      .reverse()
      .reduce(function (acc, num, i) {
        return num + (num != '-' && i && !(i % 3) ? ',' : '') + acc
      }, '') +
    '.' +
    p[1]
  )
}

const incomeValidator = (input) => {
  if (isNaN(input)) {
    return 'Please enter a number'
  }
  if (input < 0) {
    return 'Please enter a number greater than zero'
  }
  return true
}

module.exports = {
  formatDollar,
  incomeValidator,
}
