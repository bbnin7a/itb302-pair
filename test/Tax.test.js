const assert = require('assert')
const mocha = require('mocha')
const { describe, it } = mocha

const Tax = require('../lib/Tax')

describe('Tax', () => {
  describe('(Single) Caculate Tax payable with given Income ', () => {
    it('Should return $0, when income is $0', () => {
      const tax = new Tax(0)
      assert.equal(tax.tax, 0)
    })
    it('Should return $0, when income is $138,900', () => {
      const tax = new Tax(138900)
      assert.equal(tax.tax, 0)
    })

    it('Should return $1, when income is $139,000', () => {
      const tax = new Tax(139000)
      assert.equal(tax.tax, 1)
    })
    it('Should return $3,760, when income is $240,000', () => {
      const tax = new Tax(240000)
      assert.equal(tax.tax, 3760)
    })
    it('Should return $177,500, when income is $1,300,000', () => {
      const tax = new Tax(1300000)
      assert.equal(tax.tax, 177500)
    })
    it('Should return $324,300, when income is $2,180,000', () => {
      const tax = new Tax(2180000)
      assert.equal(tax.tax, 324300)
    })
  })

  describe('(Joint) Caculate Tax payable with given Income ', () => {
    it('Should return $0, when income is $0 and $0', () => {
      const tax = new Tax(0, 0)
      assert.equal(tax.tax, 0)
    })
    it('Should return $3,763, when income is $139,000 and $240,000', () => {
      const tax = new Tax(139000, 240000)
      assert.equal(tax.tax, 3763)
    })
    it('Should return $126,508.5, when income is $139,000 and $1,000,000', () => {
      const tax = new Tax(139000, 1000000)
      assert.equal(tax.tax, 126508.5)
    })
    it('Should return $516,600, when income is $1,300,000 and $2,180,000', () => {
      const tax = new Tax(1300000, 2180000)
      assert.equal(tax.tax, 516600)
    })
    it('Should return $196,200, when income is $1,200,000 and $360,000', () => {
      const tax = new Tax(1200000, 360000)
      assert.equal(tax.tax, 196200)
    })
    it('Should return $157,440, when income is $1,200,000 and $120,000', () => {
      const tax = new Tax(1200000, 120000)
      assert.equal(tax.tax, 157440)
    })
  })
})
