const assert = require('assert')
const mocha = require('mocha')
const TaxAdvisor = require('../lib/TaxAdvisor')
const Tax = require('../lib/Tax')
const { describe, it } = mocha

describe('Tax Advisor', () => {
  describe('Recommend Joint asssessment', () => {
    it('Should recommend `Joint` when income is $138,900 and $360,000', () => {
      const income1 = 138900
      const income2 = 360000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, 'Joint')
    })
    it('Should recommend `Joint` when income is $123,000 and $150,000', () => {
      const income1 = 123000
      const income2 = 150000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, 'Joint')
    })
    it('Should recommend `Joint` when income is $1,200,000 and $120,000', () => {
      const income1 = 1200000
      const income2 = 120000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, 'Joint')
    })
  })
  describe('Recommend Separate asssessment', () => {
    it('Should recommend `Separate` when income is $870,000 and $900,000', () => {
      const income1 = 870000
      const income2 = 900000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, 'Separate')
    })
    it('Should recommend `Separate` when income is $1,300,000 and $2,180,000', () => {
      const income1 = 1300000
      const income2 = 2180000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, 'Separate')
    })
  })
  describe('No recommendation', () => {
    it('Should recommend `null` when income is $100,000 and $100,000', () => {
      const income1 = 100000
      const income2 = 100000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, null)
    })
    it('Should recommend `null` when income is $5,000,000 and $5,000,000', () => {
      const income1 = 5000000
      const income2 = 5000000
      const t1 = new Tax(income1)
      const t2 = new Tax(income2)
      const joint = new Tax(income1, income2)
      const taxAdvisor = new TaxAdvisor(joint.tax, t1.tax + t2.tax)

      assert.equal(taxAdvisor.recommend, null)
    })
  })
})
