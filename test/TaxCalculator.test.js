const assert = require('assert')
const mocha = require('mocha')
const { describe, it } = mocha
const taxCalculator = require('../lib/TaxCalculator')

describe('Tax Calculator Unit Function', () => {
  describe('MPF', () => {
    it('should return $0 when income = $0', () => {
      const income = 0
      const result = taxCalculator.calcMPF(income)
      assert.equal(result, 0)
    })
    it('should return $0 when income <= $85,200', () => {
      const income = 7100 * 12
      const result = taxCalculator.calcMPF(income)
      assert.equal(result, 0)
    })
    it('should return 5% of income when $85,200 < income@$359,999 < $360,000', () => {
      const income = 359999
      const result = taxCalculator.calcMPF(income)
      assert.equal(result, income * 0.05)
    })
    it('should return `MPF Capped at $18,000` when $360,000 <= income@$360,000', () => {
      const income = 360000
      const result = taxCalculator.calcMPF(income)
      assert.equal(result, 18000)
    })
  })

  describe('Net Total Income', () => {
    it('should return subtraction of `income` and `mpf`', () => {
      const income = 138900
      const mpf = 6945
      const result = taxCalculator.calcNetTotalIncome(income, mpf)
      assert.equal(result, income - mpf)
    })
  })

  describe('Net Chargeable Income', () => {
    it('should return $0, when `netTotalIncome` is $131,955', () => {
      const netTotalIncome = 131955
      const expected = 0
      const result = taxCalculator.calcNetChargeableIncome(netTotalIncome)
      assert.equal(result, expected)
    })
    it('should return $210,000, when `netTotalIncome` is $342,000 and `allowance in single`', () => {
      const netTotalIncome = 342000
      const expected = 210000
      const result = taxCalculator.calcNetChargeableIncome(netTotalIncome)
      assert.equal(result, expected)
    })
    it('should return $209,955, when `netTotalIncome` is $131,955 and $342,000` and `allowance in marriage`', () => {
      const netTotalIncome1 = 131955
      const netTotalIncome2 = 342000
      const expected = 209955
      const result = taxCalculator.calcNetChargeableIncome(
        netTotalIncome1 + netTotalIncome2,
        true
      )
      assert.equal(result, expected)
    })
  })
  describe('Tax Payable', () => {
    it('should return standard rate 15% of `netChargeableIncome(no allowance)`', () => {
      const netChargeableIncome = 473955
      const expected = 71093.25
      const result = taxCalculator.calcTaxInStandardRate(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return progressive rate (2%) of `netChargeableIncome`', () => {
      const netChargeableIncome = 50
      const expected = 1
      const result = taxCalculator.calcTaxInProgressiveRate(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return progressive rate (6%) of `netChargeableIncome`', () => {
      const netChargeableIncome = 96000
      const expected = 3760
      const result = taxCalculator.calcTaxInProgressiveRate(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return progressive rate (10%) of `netChargeableIncome`', () => {
      const netChargeableIncome = 140650
      const expected = 8065
      const result = taxCalculator.calcTaxInProgressiveRate(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return progressive rate (14%) of `netChargeableIncome`', () => {
      const netChargeableIncome = 162500
      const expected = 10750
      const result = taxCalculator.calcTaxInProgressiveRate(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return progressive rate (17%) of `netChargeableIncome`', () => {
      const netChargeableIncome = 210000
      const expected = 17700
      const result = taxCalculator.calcTaxInProgressiveRate(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return tax payable of `netChargeableIncome(no allwance)` is high (Standard Rate)', () => {
      const netChargeableIncome = 2162000
      const expected = 324300
      const result = taxCalculator.calcTaxPayable(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return tax payable of `netChargeableIncome` is medium (Progressive)', () => {
      const netChargeableIncome = 850000
      const expected = 126500
      const result = taxCalculator.calcTaxPayable(netChargeableIncome)
      assert.equal(result, expected)
    })
    it('should return tax payable of `netChargeableIncome` is low (Progressive)', () => {
      const netChargeableIncome = 50400
      const expected = 1024
      const result = taxCalculator.calcTaxPayable(netChargeableIncome)
      assert.equal(result, expected)
    })
  })
})
